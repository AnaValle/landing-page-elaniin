/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);


export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            component: () => import(/* webpackChunkName: "Foodies" */'@/container/Foodies.vue'),
            redirect:'/home',
            children: [{
                path: 'home',
                name: 'home',
                component: () => import(/* webpackChunkName: "Home" */'@/views/Home.vue'),
            }, {
                path: 'menu',
                name: 'menu',
                component: () => import(/* webpackChunkName: "Menu" */'@/views/MainMenu.vue'),
            }
            ],
        },

        // {
        //     path: '*',
        //     name: '404',
        //     component: () => import(/* webpackChunkName: "404" */'@/views/error-pages/404.vue'),
        //     beforeEnter: async (to, from, next) => {
        //         next();
        //         removeLoader();
        //     },
        // },
    ],
});
