import axios from 'axios';
// import store from '@/store/store.js';
// import router from '@/router/router.js';

export const baseURL = 'https://api.elaniin.dev/';

export const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
};

/** @type AxiosInstance */
export const api = axios.create({
    baseURL,
    headers,
});


api.defaults.headers.post['Content-Type'] = 'application/json';
api.defaults.headers.post.Accept = 'application/json';
