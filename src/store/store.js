/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';


const files = require.context('./modules/.', true, /index.js/);
const modules = {};

files.keys().forEach((fileName) => {
    if (fileName === 'index.js') return;
    const moduleName = fileName
        .replace(/(\.\/|\.js)/g, '')
        .replace(/^[a-z]{1}/, x => x.toUpperCase())
        .replace(/\/index/, '');
    modules[moduleName] = files(fileName).default;
});

Vue.use(Vuex);

export default new Vuex.Store({
    modules,
});
