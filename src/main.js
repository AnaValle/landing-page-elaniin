import Vue from 'vue'
import App from './App.vue'
import './assets/styles/index.css';
import {api, baseURL} from '@/apiConf';

import router from './router/router';
import store from './store/store';

import * as VueGoogleMaps from 'vue2-google-maps';


let VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCmA1WfaUVwN2qQR8KvoGmuaM0qqdAWUAA',
    libraries: 'places, visualization, drawing', // This is required if you use the Autocomplete plugin
  },
});

Vue.prototype.$baseURL = baseURL;
Vue.prototype.$api = api;

export const eventBus = new Vue(); // Global event bus

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
