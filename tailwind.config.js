module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            backgroundImage: theme => ({
                'foodies-pattern1': "url(/img/Subtract.svg)",
                'about-us-pattern': "url(/img/Vector3.svg)",
                'about-us-background':"linear-gradient(359.79deg, #040404 -13.79%, rgba(0, 0, 0, 0) 61.58%), url(/img/aGZeHyx-jfo.svg)",
                'comments-background1':"url(/img/Commentbg1.svg)",
                'comments-background2':"url(/img/Commentbg2.svg)",
            }),
            height: theme => ({
                'comments-height':"35rem"
            })
        },

    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
};
